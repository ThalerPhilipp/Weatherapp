import React from "react";

export default function Footer(props) {
  return (
    <div className="footer">
      <footer>
        <h3>
          Philipp Thaler {"\u00A9"}
          {new Date().getFullYear()}
        </h3>
      </footer>
    </div>
  );
}
