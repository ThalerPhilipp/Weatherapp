import React from "react";
import { Link } from "react-router-dom";
import WeatherSearch from "./WeatherSearch";
import {
  MobileBreakpoint,
  DesktopBreakpoint
} from "./responsive_utils/Breakpoint";
import "../css/Navbar.css";
import Button from "./navigation/Button";
const searchIcon = require("../images/search-solid.svg");

const inputStyles = {
  form: {
    display: "flex",
    flexDirection: "row",
    padding: "20px",
    minWidth: "400px"
  },
  input: {
    height: "50px",
    width: "100%",
    display: "block"
  },
  button: {
    padding: "9px",
    paddingBottom: "10px",
    backgroundColor: "aliceblue",
    color: "black",
    textDecoration: "none",
    borderStyle: "solid",
    borderWidth: "1px"
  }
};

export default function Navbar(props) {
  return (
    <div>
      <MobileBreakpoint>
        <div className="navbar">
          <div className="toprow">
            <Button type="back" />
            <Link to="/" className="logo">
              <h1>Weatherapp</h1>
            </Link>
            <Button type="forward" />
          </div>
          {/* This hides the searchbar in the navbar on the startpage when on mobile */}
          {window.location.pathname === "/" ? null : (
            <div className="headerSearch">
              <WeatherSearch styles={inputStyles}>
                <img src={searchIcon} height="25px" />
              </WeatherSearch>
            </div>
          )}
        </div>
      </MobileBreakpoint>

      <DesktopBreakpoint>
        <div className="navbar">
          <Link to="/" className="logo">
            <h1>Weatherapp</h1>
          </Link>
          <div className="headerSearch">
            <WeatherSearch styles={inputStyles}>
              <img src={searchIcon} height="25px" />
            </WeatherSearch>
          </div>
        </div>
      </DesktopBreakpoint>
    </div>
  );
}
