import React from "react";
import PropTypes from "prop-types";
import "../css/ForecastDay.css";

const icons = require.context("../images/weather-icons", true, /\.svg$/);
const paths = icons.keys();
const svgs = paths.map(path => icons(path));

export default function IconComponent(props) {
  const dateOptions = {
    weekday: "long",
    month: "short",
    day: "numeric"
  };
  const date = new Date(props.data.dt * 1000).toLocaleDateString(
    "en-US",
    dateOptions
  );
  return (
    <div className="day-container">
      <img src={svgs[svgs.indexOf(`/images/${props.data.weather.icon}.svg`)]} />
      <h2 className="subheader">{date}</h2>
    </div>
  );
}

IconComponent.propTypes = {
  data: PropTypes.object.isRequired
};
