import React from "react";

export default function NotFound(props) {
  return (
    <div className="home-container">
      <h1>404 Page not found!!!</h1>
    </div>
  );
}
