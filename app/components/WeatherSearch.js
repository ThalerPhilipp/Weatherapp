import React from "react";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";

const defaultStyles = {
  form: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    padding: "20px"
  },
  input: {
    width: "100%",
    minWidth: "170px",
    height: "50px"
  },
  button: {
    marginTop: "10px",
    backgroundColor: "lightblue",
    borderRadius: "15px",
    width: "10%",
    minWidth: "120px",
    color: "black",
    textDecoration: "none"
  }
};

export default class WeatherSearch extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      location: "",
      styles: this.props.styles
    };

    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    this.setState({ location: event.target.value });
  }

  render() {
    const { location, styles } = this.state;

    return (
      <div>
        <form style={styles.form}>
          <input
            style={styles.input}
            id="location"
            placeholder="New York, Plangeross..."
            type="text"
            autoComplete="off"
            onChange={this.handleChange}
            onSubmit={e => {
              e.preventDefault();
            }}
          />

          <Link
            style={styles.button}
            type="button"
            to={`/forecast?city=${location}`}
          >
            <div>{this.props.children}</div>
          </Link>
        </form>
      </div>
    );
  }
}

WeatherSearch.propTypes = {
  location: PropTypes.string,
  styles: PropTypes.object.isRequired
};

WeatherSearch.defaultProps = {
  styles: defaultStyles
};
