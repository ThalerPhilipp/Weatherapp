import axios from "axios";

const API_KEY = "073c57683e1c8161b3f2c4fe9b23eeeb";

function getCurrentWeather(location) {
  return axios
    .get(
      `https://api.openweathermap.org/data/2.5/weather?q=${location}&type=accurate&APPID=${API_KEY}`
    )
    .then(data => data)
    .catch(handleError);
}

function getForecast(location) {
  return axios
    .get(
      `https://api.openweathermap.org/data/2.5/forecast?q=${location}&type=accurate&APPID=${API_KEY}`
    )
    .then(location => location)
    .catch(handleError);
}

function DayFactory(forecast) {
  return {
    city: forecast.city.name,
    dt: 0,
    dt_txt: "",
    main: {
      grnd_level: 0,
      humidity: 0,
      pressure: 0,
      sea_level: 0,
      temp: 0,
      temp_kf: 0,
      temp_max: 0,
      temp_min: 0
    },
    weather: {
      main: "",
      description: "",
      icon: ""
    },
    wind: {
      deg: 0,
      speed: 0
    }
  };
}

function getMostFrequent(array) {
  let mf = 1;
  let m = 0;
  let item;

  for (let i = 0; i < array.length; i++) {
    for (let j = i; j < array.length; j++) {
      if (array[i] == array[j]) m++;
      if (mf < m) {
        mf = m;
        item = array[i];
      }
    }
    m = 0;
  }
  if (!item) {
    item = array[0];
  }

  return item;
}

// The Openweathermap API only provides 3 hour forecasts, so this function will calculate the average and return an array of the days
// Pretty messy !
function getDailyForecast(forecast) {
  const list = forecast.list;
  let i = 0,
    counter = 0;
  let days = [];
  let date = forecast.list[0].dt_txt.substring(0, 10);
  let day = DayFactory(forecast);
  let mainCount = [];
  let descCount = [];
  let iconCount = [];
  list.forEach(element => {
    counter++;

    if (element.dt_txt.includes("12:00:00")) {
      day["dt"] = element.dt;
      day["dt_txt"] = element.dt_txt;
      day["weather"]["main"] = element.weather[0].main;
      day["weather"]["description"] = element.weather[0].description;
      day["weather"]["icon"] = element.weather[0].icon;
    }
    day["main"]["grnd_level"] += element.main.grnd_level;
    day["main"]["humidity"] += element.main.humidity;
    day["main"]["pressure"] += element.main.pressure;
    day["main"]["sea_level"] += element.main.sea_level;
    day["main"]["temp"] += element.main.temp;
    day["main"]["temp_kf"] += element.main.temp_kf;
    day["main"]["temp_max"] += element.main.temp_max;
    day["main"]["temp_min"] += element.main.temp_min;
    mainCount.push(element.weather[0].main);
    descCount.push(element.weather[0].description);
    iconCount.push(element.weather[0].icon);
    day["wind"]["deg"] += element.wind.deg;
    day["wind"]["speed"] += element.wind.speed;
    // If the day changes, the next items come inside the next index of the array
    if (element.dt_txt.substring(0, 10) !== date) {
      if (!day["dt"]) {
        day["dt"] = element.dt;
        day["dt_txt"] = element.dt_txt;
      }
      day["main"]["grnd_level"] /= counter;
      day["main"]["humidity"] /= counter;
      day["main"]["pressure"] /= counter;
      day["main"]["sea_level"] /= counter;
      day["main"]["temp"] /= counter;
      day["main"]["temp_kf"] /= counter;
      day["main"]["temp_max"] /= counter;
      day["main"]["temp_min"] /= counter;
      day["wind"]["deg"] /= counter;
      day["wind"]["speed"] /= counter;
      if (!day["weather"]["main"]) {
        day["weather"]["main"] = getMostFrequent(mainCount);
        day["weather"]["description"] = getMostFrequent(descCount);
        day["weather"]["icon"] = getMostFrequent(iconCount);
        if (day["weather"]["icon"].endsWith("n")) {
          day["weather"]["icon"] = day["weather"]["icon"].replace("n", "d");
        }
      }
      counter = 0;
      days[i] = day;
      i++;
      date = element.dt_txt.substring(0, 10);
      day = DayFactory(forecast);
    }
  });

  return days;
}

function handleError(error) {
  console.error(error);
  return null;
}

export default { getCurrentWeather, getForecast, getDailyForecast };
